# Learning git

A simple challenge in git branching to familiarize yourself with git commands. Can be completed after reading through this presentation: https://docs.google.com/presentation/d/1gWJ5W0i7okAEWCYxRFZxYUvDexA0PMPehyMtRZbSiVA/edit?usp=sharing

# Procedure (warning: spoilers)

## 1: Cloning the repo

    git clone https://gitlab.com/John.alessio/learning-git.git

This will clone into your current directory, use the 'cd' command to get to an easily accessible location such as your desktop or documents folder. Use the 'pwd' command to see your current directory.

for the rest of the guide, make sure to be in the directory of the repository ("learning-git")

## 2: Create a new branch and switch to it
```
git branch <branch name>
git checkout <branch name>
```
or use the shortcut...

    git checkout -b <branch name>
make the branch name submission/\<your name\>

## 3: Copy, rename, fill out the quiz, and place in submissions folder
you can do this using notepad and your computers desktop interface, or if you want you can do it via the terminal. If using the terminal, look into the commands "cp", "mv", and "nano".

 ## 4: Stage changes

     git add <file name>
check your commits status using:

    git status
 ## 5: Commit changes
 

    git commit -m "<message>"

## 6: Push current branch to remote branch 

Push current branch without setting an upstream branch:

    git push origin <remote branch name>

or...
set upstream branch for the current branch then push:
```
git branch --set-upstream-to=origin/<remote branch name>
git push
```
or...
Shortcut to set upstream and push

    git push --set-upstream origin <remote branch name>
## 7: Create a merge request!
You can do this on the gitlab website for this repository (https://gitlab.com/John.alessio/learning-git)
